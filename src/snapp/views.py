from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from snapp.models import *

# Create your views here.
def index(request):
   lines = Line.objects.all()
   template = loader.get_template('snapp/index.html')
   context = {'lines': lines}
   return HttpResponse(template.render(context))

def timetable(request, line_id):
   line = Line.objects.get(id=line_id)
   stations = line.station_set.all().order_by('order')
   days = Day.objects.all()
   tacts = Tact.objects.all().order_by('end')
   template = loader.get_template('snapp/timetable.html')
   d = 0
   for station in stations:
       d += station.duration
       station.whole_duration = d

   context = {
       'stations': stations, 
       'line': line,
       'days': days,
       'tacts': tacts
    }
   return HttpResponse(template.render(context))

def schedule(request):
    available_personals = Personal.objects.all().filter(absence='A').filter(assignment__isnull=True)
    trains = Train.objects.all()
    available_locomotive = Locomotive.objects.all().filter(availability=True)
    available_wagons = Wagon.objects.all().filter(availability=False)
    available_wagon_1 = available_wagons
    train = Train(
        locomotive = available_locomotive.first(),
    ) 
    wagon = Wagon(
        train = train
    )
    #for train in trains:

    #trains = Train.objects.all()
    #lines = Line.objects.all()
    template = loader.get_template('snapp/schedule.html')
    context = {
        'personals': available_personals,
        'trains': trains,
        'train': train
    }
    return HttpResponse(template.render(context))