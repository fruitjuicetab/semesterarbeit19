from django.contrib import admin
from .models import *

class StationAdmin(admin.ModelAdmin):       
    def save_model(self, request, obj, form, change):
        user = request.user 
        instance = form.save(commit=False)
        #if not instance.distance:
        instance.distance = (instance.duration/60)*50
        instance.save()
        form.save_m2m()
        return instance

admin.site.register(Personal)
admin.site.register(Train)
admin.site.register(Wagon)
admin.site.register(Locomotive)
admin.site.register(Day)
admin.site.register(Line)
admin.site.register(Station, StationAdmin)
admin.site.register(Tact)
