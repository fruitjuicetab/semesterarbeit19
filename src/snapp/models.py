from django.db import models
from datetime import date, datetime, timedelta

# Create your models here.
# These models define the train
class Train(models.Model):
    train_number = models.PositiveIntegerField()
    def __str__(self):
        return "{}".format(self.train_number)
        

 # This model is an abstract class (a superclass from locomotive and wagon)   
class RollingStock(models.Model):
    rolling_number = models.PositiveIntegerField()
    launch = models.DateField()
    last_revision = models.DateField()
    next_revision = models.DateField()
    availability = models.BooleanField(default=True)
    def __str__(self):
        return "{}".format(self.rolling_number)
    
    class Meta:
        abstract = True

# This class references the train class as a one to one referential
class Locomotive(RollingStock):
    train = models.OneToOneField(
        Train,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

# This class references the train class with a foreign key as a one to many referential
class Wagon(RollingStock):
    grading = models.PositiveIntegerField()
    places = models.PositiveIntegerField()
    train = models.ForeignKey(Train, on_delete=models.CASCADE, null=True, blank=True)
# These models define the timetable
class Line(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Station(models.Model):
    name = models.CharField(max_length=50)
    order = models.IntegerField()
    mainstation = models.BooleanField()
    duration = models.IntegerField()
    holding_time = models.IntegerField(default=2)
    distance = models.IntegerField(default=0)
    line = models.ForeignKey(Line, on_delete=models.CASCADE)
    def __str__(self):
        return self.name

class Day(models.Model):
    name = models.CharField(max_length=10)
    start = models.TimeField()
    end = models.TimeField()
    def __str__(self):
        return self.name
    def to_datetime(self, time):
        return (datetime.combine(date.today(), time))
    #dokumentieren
    @property
    def get_times(self):
        t = self.to_datetime(self.start)
        times = [self.start]
        for tact in self.tact_set.all():
            end = self.to_datetime(tact.end)
            while t < end:
                t += timedelta(minutes=tact.interval) 
                times.append(t.time())
        return times

class Tact(models.Model):
    interval = models.IntegerField()
    end = models.TimeField()
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    def __str__(self):
        return "{} Interval: {}min Ende: {}".format(self.day,self.interval,self.end)
    
class Assignment(models.Model):
    train = models.OneToOneField(
    Train,
    on_delete=models.CASCADE,
    )
 
# This model define the personal
class Personal(models.Model):
    # Define the key value. Left save in the database and right one is displayed
    PRESENT = 'A'
    ABSENCE_TYPES = (
        (PRESENT, 'Anwesend'),
        ('K', 'Krank'),
        ('F', 'Ferien'),
    )
    CATEGORY_TYPE = (
        ('L', 'Lokomotivführer'),
        ('K', 'Kontrolleur')
    )
    personal_number = models.PositiveIntegerField()
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    personal_category = models.CharField(max_length=20, choices=CATEGORY_TYPE)
    absence = models.CharField(max_length=10,choices=ABSENCE_TYPES, default=PRESENT)
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE, null=True, blank=True)
    # return humanreadable string for object
    def __str__(self):
        return "{}".format(self.personal_number)        

    






    
