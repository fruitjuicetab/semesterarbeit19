# Generated by Django 2.1.7 on 2019-03-17 13:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snapp', '0007_auto_20190317_1103'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locomotive',
            name='availability',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='wagon',
            name='availability',
            field=models.BooleanField(default=True),
        ),
    ]
