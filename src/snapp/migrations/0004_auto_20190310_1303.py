# Generated by Django 2.1.7 on 2019-03-10 13:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snapp', '0003_station_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='station',
            name='order',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='station',
            name='distance',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='station',
            name='holding_time',
            field=models.IntegerField(default=2),
        ),
    ]
