@startuml
left to right direction

rectangle  {
(Streckennetz anzeigen) as SnAnz
(Streckennetz bearbeiten) as SnBear

(Rollmaterial verwalten) as RmVerw
(Personal verwalten) as PeVerw

(Fahrplan generieren) as FpGen
(Fahrplan anzeigen) as FpAnz
(Einsatzplan generieren) as EpGen
(Einsatzplan anzeigen) as EpAnz
}
actor Personal
actor "Lokomotivf�hrer/in" as Loko
actor "Kontrolleur/in" as Kontrolleur
actor Administrator  
actor Kunde 
Loko --|> Personal
Kontrolleur --|> Personal


Personal --> SnAnz
Administrator --> SnBear
Administrator --> RmVerw
Administrator --> PeVerw
Administrator --> FpGen
Administrator --> FpAnz
Personal --> FpAnz
FpAnz <-- Kunde 
Administrator --> EpGen
Administrator --> EpAnz
Personal --> EpAnz

@enduml